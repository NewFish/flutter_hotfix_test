package com.example.flutter_hotfix_test

import com.tencent.tinker.loader.app.TinkerApplication
import com.tencent.tinker.loader.shareutil.ShareConstants


class HotFixApplication() : TinkerApplication(ShareConstants.TINKER_ENABLE_ALL, "com.example.flutter_hotfix_test.SampleApplicationLike",
        "com.tencent.tinker.loader.TinkerLoader", false) {
}